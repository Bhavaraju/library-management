import { Component, OnInit } from '@angular/core';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { ActivatedRoute, Router } from '@angular/router';
import {CommonDataServiceService} from '../services/common-data-service.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private router: Router,private _globalData:CommonDataServiceService) { }
    usersData:any = [];
    loginUSer:any;
  ngOnInit() {
    this.usersData = JSON.parse(localStorage.getItem("userData") ) ;
    this.loginUSer =localStorage.getItem("loginUser")
    console.log(this.usersData);
  }
  goToEdit(booksData,Index){
    this._globalData.selectedIndex = Index;
    this._globalData.selectedBook = booksData;
    
    this.router.navigate(['/editUser']);
  }

  deleteUser (index ) {
    let userData = JSON.parse(localStorage.getItem("userData") )
    userData.splice(index,1)
    localStorage.setItem('userData',JSON.stringify(userData))

    this.usersData = JSON.parse(localStorage.getItem("userData") )
  }

}
