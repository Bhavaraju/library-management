import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonDataServiceService {

  constructor() { }
  selectedUSer:any;
  selectedBook:any;
  selectedIndex:any;
  booksData:any = [
    {
      name:'Alfreds Futterkiste',
      author:'Maria Anders',
      datePublished:'1-10-2018',
      qty:'10',
    },
    {
      name:'Island Trading',
      author:'Helen Bennett',
      datePublished:'1-10-2018',
      qty:'90',
    },
    {
      name:'Ernst Handel',
      author:'Roland Mendel',
      datePublished:'5-10-2018',
      qty:'10',
    },
    {
      name:'Centro comercial Moctezuma',
      author:'Francisco Changs',
      datePublished:'10-10-2018',
      qty:'70',
    }
  ]
  usersData:any = [
    {firstName:"Sairam",lastName:"Bhavaraju",email:"sairam@gmail.com",phone:634789236478,signUpdate:"1-1-18",lastLogin:"",password:"test",type:"1"},
    {firstName:"gopal",lastName:"vms",email:"gopal@gmail.com",phone:276347623,signUpdate:"2-1-18",lastLogin:"",password:"test",type:"2"},
    {firstName:"Raju",lastName:"Rjx",email:"raju@gmail.com",phone:216378126,signUpdate:"3-1-18",lastLogin:"",password:"test",type:"2"}
  ]
}
