import { Component, OnInit } from '@angular/core';
import {CommonDataServiceService} from '../services/common-data-service.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-edit-book-list',
  templateUrl: './edit-book-list.component.html',
  styleUrls: ['./edit-book-list.component.css']
})
export class EditBookListComponent implements OnInit {

  selectedBook:any;
  selectedinx:any;
  name:any;
  author:any;
  qty:any;
  constructor(private _globalData:CommonDataServiceService,public router:Router) { }
  ngOnInit() {
    this.selectedBook = this._globalData.selectedBook;
    this.selectedinx = this._globalData.selectedIndex;
    console.log(this.selectedBook);
    console.log(this.selectedinx);
    
  }

  saveBook () {
    let booksData = JSON.parse(localStorage.getItem("booksData") ) ; 
    console.log(booksData)
    booksData[this.selectedinx].name = this.selectedBook.name;
    booksData[this.selectedinx].lastName = this.selectedBook.author;
    booksData[this.selectedinx].qty = this.selectedBook.qty;
    localStorage.setItem('booksData',JSON.stringify(booksData))
    this.router.navigate(['/dashboard/books'])
  }
}
