import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBookListComponent } from './edit-book-list.component';

describe('EditBookListComponent', () => {
  let component: EditBookListComponent;
  let fixture: ComponentFixture<EditBookListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBookListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBookListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
