// import { Component, OnInit } from '@angular/core';
import { Component, ViewEncapsulation,OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http'
// import { LocalStorageService } from 'angular-2-local-storage';


@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {

  
  public form:FormGroup;
  public username:AbstractControl;
  public password:AbstractControl;
  public registerCredentials = { username: '', password: '' };
  // login: Login = new Login();
  loginError:any = false;
  app_mode="PROD_mode";
  enable=true;
  userData:any=[];
  bookData:any=[];

  @ViewChild('keepLog') keepLogin;

  constructor(public router:Router, 
              fb:FormBuilder
            ) {
     
      
    
      //this statment only for demo not complete valdation
      this.form = new FormGroup({
            'username': new FormControl('', Validators.required), 
            "password": new FormControl('', Validators.required),
        })
  }

  ngOnInit(){
    this.bookData = JSON.parse(localStorage.getItem("booksData") ) ;
    this.userData = JSON.parse(localStorage.getItem("userData") ) ;    
  }

  onSubmit():void{
    console.log(this.userData)
    this.username = this.form.controls['username'].value;
    this.password = this.form.controls['password'].value;
    for(let i=0;i <this.userData.length;i++){
      console.log(this.userData[i])
      if(this.userData[i].password == this.password && this.userData[i].email == this.username){
        this.router.navigate(['/dashboard/users']);
        this.userData[i].lastLogin = new Date()
        localStorage.setItem('loginUser',this.userData[i].type)
        localStorage.setItem('userData',JSON.stringify(this.userData))

      }
    }
   console.log('uer',this.username)
   console.log('uer',this.password)
  }

}
