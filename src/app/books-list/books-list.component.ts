import { Component, OnInit } from '@angular/core';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { ActivatedRoute, Router } from '@angular/router';
import {CommonDataServiceService} from '../services/common-data-service.service';

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.css']
})
export class BooksListComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private router: Router,private _globalData:CommonDataServiceService) { }
    booksListData:any = [];
    loginUSer:any;
  ngOnInit() {
    this.loginUSer =localStorage.getItem("loginUser")
    this.booksListData =  JSON.parse(localStorage.getItem("booksData") ) ;
    console.log(this.booksListData);
  }
  goToEdit(booksData,Index){
    this._globalData.selectedIndex = Index;
    this._globalData.selectedBook = booksData;
    this.router.navigate(['/editBook']);
  }
  
  deleteBook (index ) {
    let booksData = JSON.parse(localStorage.getItem("booksData") )
    booksData.splice(index,1)
    localStorage.setItem('booksData',JSON.stringify(booksData))

    this.booksListData = JSON.parse(localStorage.getItem("booksData") )
  }
}
