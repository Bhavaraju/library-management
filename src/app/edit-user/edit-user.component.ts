import { Component, OnInit } from '@angular/core';
import {CommonDataServiceService} from '../services/common-data-service.service';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  selectedUser:any;
  selectedinx:any;
  firstname:any;
  lastname:any;
  phone:any;
  constructor(private _globalData:CommonDataServiceService,public router:Router) { }
  ngOnInit() {
    this.selectedUser = this._globalData.selectedBook;
    this.selectedinx = this._globalData.selectedIndex;
    console.log(this.selectedUser);
    console.log(this.selectedinx);
    
  }

  saveUser () {
    let userData = JSON.parse(localStorage.getItem("userData") ) ; 
      userData[this.selectedinx].firstName = this.selectedUser.firstName;
      userData[this.selectedinx].lastName = this.selectedUser.lasttName;
      userData[this.selectedinx].phone = this.selectedUser.phone;
      localStorage.setItem('userData',JSON.stringify(userData))
      this.router.navigate(['/dashboard/users'])
  }


  

}
