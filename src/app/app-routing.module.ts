import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponentComponent } from './login-component/login-component.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { UserListComponent } from './user-list/user-list.component';
import { BooksListComponent } from './books-list/books-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { EditBookListComponent } from './edit-book-list/edit-book-list.component';




const routes: Routes = [
  {path:'',pathMatch:'full' ,redirectTo:'login'},
  { path: 'login', component: LoginComponentComponent },
  { path: 'signUp', component: SignUpComponent },
  {path:'dashboard',component:DashboardComponent ,children:[
    {path:'users',component:UserListComponent},
    {path:'books',component:BooksListComponent}
  ]},
  {path:'editUser',component:EditUserComponent},
  {path:'editBook',component:EditBookListComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
