import { Component ,OnInit} from '@angular/core';
import {CommonDataServiceService} from './services/common-data-service.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'LibraryManagemetn';
  userData:any;
  booksData:any;
  constructor(private dataService:CommonDataServiceService){

  }

  ngOnInit() {
    this.userData = this.dataService.usersData
    localStorage.setItem('userData',JSON.stringify(this.userData))
    this.booksData =this.dataService.booksData;
    localStorage.setItem("booksData",JSON.stringify(this.booksData))
  }
}
