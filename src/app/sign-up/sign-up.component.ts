import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators,FormArray} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  public myform:any;
  constructor(public router:Router) { }

  ngOnInit() {

    //Intialising form validations
    this.myform = new FormGroup({
      'firstname': new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.pattern("[a-zA-Z ]*"),
      ]),
      'lastname': new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.pattern("[a-zA-Z ]*"),
      ]),
      
      'phonenumber': new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.pattern("[0-9]*"),
      ]),
      'email': new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'),
      ]),
      'password': new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      'type': new FormControl('', [
        Validators.required,
        
      ])
    })

    
  }
  
  //Form submition method
  onSubmit() {
    let userData =JSON.parse(localStorage.getItem("userData") )
    let Obj = {
      firstName:this.myform.controls['firstname'].value,
      lastName:this.myform.controls['lastname'].value,
      phone:this.myform.controls['phonenumber'].value,
      email:this.myform.controls['email'].value,
      password:this.myform.controls['password'].value,
      type:this.myform.controls['type'].value,
      signUpdate:new Date(),
      lastLogin:""

    }
    userData.push(Obj)
    localStorage.setItem('userData',JSON.stringify(userData))
    this.router.navigate(['/login'])
    console.log('form',this.myform)
  }
}